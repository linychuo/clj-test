(def a (/ 1.0 3.0))
(def b (/ 3.0 1.0))
(println a b (* a b))
(def c (* a a a a a a))
(def d (* b b b b b b))
(println (* c d))

(defn list-xyz [xyz-map]
  (list (:x xyz-map) (:y xyz-map) (:z xyz-map)))

(println (list-xyz {:x 1, :y 2, :z 3}))

(defn list-xyz [{x :x, y :y, z :z}]
  (list x y z))
(println (list-xyz {:x 1, :y 2, :z 3}))

(defn list-xyz [{:keys [x, y, z]}]
  (list x y z))

(println (list-xyz {:x 1, :y 2, :z 3}))


;how to write x = x + 1
(def x (atom 0))
(println (swap! x + 1))
;a more idiomatic way
(def x (atom 4))
(println (swap! x inc))

;set
(def a #{1, 2, 3})
(def b #{4, 5})
(println a b)