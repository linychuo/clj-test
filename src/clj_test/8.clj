(import '(javax.swing JFrame JLabel JTextField JButton)
        '(java.awt.event ActionListener)
        '(java.awt GridLayout))

(let [frame (new JFrame "Celsius Converter (摄氏度转华氏度)")
      temp-text (new JTextField)
      celsius-label (new JLabel "Celsius (摄氏度)")
      convert-button (new JButton "Convert (进行转换)")
      fahrenheit-label (new JLabel "Fahrenheit (华氏度)")]
  (. convert-button
     (addActionListener
       (proxy [ActionListener] []
         (actionPerformed [evt]
           (let [c (Double/parseDouble (. temp-text (getText)))]
             (. fahrenheit-label
                (setText (str (+ 32 (* 1.8 c)) " Fahrenheit (华氏度)"))))))))
  (doto frame
    ;(.setDefaultCloseOperation (JFrame/EXIT_ON_CLOSE)) ; uncomment this to quit app on frame close
    (.setLayout (new GridLayout 2 2 3 3))
    (.add temp-text)
    (.add celsius-label)
    (.add convert-button)
    (.add fahrenheit-label)
    (.setSize 300 80)
    (.setVisible true)))