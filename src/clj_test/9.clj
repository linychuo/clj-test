(defn run-report [user]
  (println "Running report for" user))

(defn dispatch-reporting-jobs [all-users]
  (doseq [user all-users]
    (run-report user)))

(dispatch-reporting-jobs ["a" "b"])

(defn range-info [x]
  (cond
    (< x 0) (println "Negative!")
    (= x 0) (println "Zero!")
    :default (println "Positive!")))

(range-info 4)
(range-info -4)
(range-info 0)


(defn chessboard-labels []
  (for [alpha "abcd" num (range 1 4)] (str alpha num)))

(println (chessboard-labels))

(println "------------------")
(defn prime? [x]
  (let [divisors (range 2 (inc (int (Math/sqrt x))))
        remainders (map #(rem x %) divisors)]
    (not (some zero? remainders))))

(defn primes-less-than [n]
  (for [x (range 2 (inc n)) :when (prime? x)] x))
(println (primes-less-than 50))

(println (range 1 (inc 10)))

(defn total-cost [item-cost number-of-item]
  (* item-cost number-of-item))

(println (total-cost 10 5))

;pre- and postcondition function
(defn item-total [price quantity]
  {:pre  [(> price 0) (> quantity 0)]
   :post [(> % 0)]}
  (* price quantity))

(println (item-total 2.3 4))

