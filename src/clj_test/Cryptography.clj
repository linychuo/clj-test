(ns clj_test.Cryptography)

(defn rand-bytes [size]
  (let [rand (java.security.SecureRandom/getInstance "SHA1PRNG")
        buffer (make-array Byte/TYPE size)]
    (.nextBytes rand buffer)
    buffer))

(defn encrypt [m]
  (let [message (.getBytes m)
        size (count message)
        pad (rand-bytes size)
        code (map bit-xor message pad)]
    {:pad (vec pad) :msg (vec code)}))

(encrypt "Attack At Down")