(def total-cost (fn [item-cost number-of-items] (* item-cost number-of-items)))

(println (total-cost 10 2))

(def users [{:username     "ivan"
             :balance      175.00
             :member-since "2001-2-6"}
            {:username     "lily"
             :balance      200.34
             :member-since "2001-4-5"}])

(println (map (fn [user] (user :member-since)) users))
(println (map #(% :member-since) users))                    ;shortcut