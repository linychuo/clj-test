(ns clj_test.atom_test)

(def current-track (atom "hello"))
(deref current-track)
@current-track

(reset! current-track "ivan")
@current-track