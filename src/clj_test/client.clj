(ns clj-test.client)

(import '(java.net Socket)
        '(java.io PrintWriter InputStreamReader BufferedReader))

(def socket (atom nil))
(def in (atom nil))
(def out (atom nil))

(defn start-client [port]
  (do
    (reset! socket (Socket. "localhost" port))
    (reset! out (PrintWriter. (. @socket getOutputStream) true))
    (reset! in (BufferedReader. (InputStreamReader. (. @socket getInputStream))))))

(defn send-message [m]
  (do
    (. @out println m)
    (println (. @in readLine))))

(defn stop-client []
  (do (. @in close)
      (. @out close)
      (. @socket close)))

(start-client 1234)
