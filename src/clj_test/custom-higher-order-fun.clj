(def users [{:username     "ivan"
             :balance      175.00
             :member-since "2001-2-6"}
            {:username     "aily"
             :balance      200.34
             :member-since "2001-4-5"}])

(defn username [user]
  (user :username))

(defn balance [user]
  (user :balance))

(defn sorter-using [ordering-fn]
  (fn [users]
    (sort-by ordering-fn users)))

(def poorest-first (sorter-using balance))
(def alphabetically (sorter-using username))

(println (poorest-first users))
(println (alphabetically users))
