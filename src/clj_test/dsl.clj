(ns clj_test.dsl)

(defmulti emit-bash
          (fn [form] (class form)))

(defmethod emit-bash
           clojure.lang.PersistentList
           [form]
  (case (name (first form))
    "println" (str "echo " (second form))
    nil))

(defmethod emit-bash
           java.lang.String
           [form]
  form)

(defmethod emit-bash
           java.lang.Long
           [form]
  (str form))

(defmethod emit-bash
           java.lang.Double
           [form]
  (str form))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def ^{:dynamic true} *current-implementation*)
(defmulti emit
          (fn [form] [*current-implementation* (class form)]))

(defmethod emit [::common java.lang.String]
           [form]
  form)

(defmethod emit [::common java.lang.Long]
           [form]
  (str form))

(defmethod emit [::common java.lang.Double]
           [form]
  (str form))

(defmethod emit [::bash clojure.lang.PersistentList]
           [form]
  (case (name (first form))
    "println" (str "echo " (second form))
    nil))

(defmethod emit [::batch clojure.lang.PersistentList]
           [form]
  (case (name (first form))
    "println" (str "ECHO " (second form))
    nil))

(defmacro script [form]
  '(emit '~form))

(defmacro with-implementation
  [impl body]
  '(binding [*current-implementation* impl]
     ~@body))

(macroexpand (with-implementation ::bash
                                  (script
                                    (println "a"))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(defn emit-bash-form [a]
;  (case (class a)
;    clojure.lang.PersistentList
;    (case (name (first a))
;      "println" (str "echo " (second a))
;      nil)
;    java.lang.String a
;    java.lang.Integer (str a)
;    java.lang.Double (str a)
;    nil))
