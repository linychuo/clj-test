(def ^:dynamic eval-me 10)                                  ;(def *eval-me* 10)
(defn print-the-var [label]
  (println label eval-me))

(print-the-var "A:")

(binding [eval-me 20]
  (print-the-var "B:")
  (binding [eval-me 30]
    (print-the-var "C:"))
  (print-the-var "D:"))

(print-the-var "E:")