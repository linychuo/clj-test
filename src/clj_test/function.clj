(defn basic-item-total [price quantity] (* price quantity))

(defn with-line-item-conditions [f price quantity]
  {:pre  [(> price 0) (> quantity 0)]
   :post [(> % 1)]}
  (apply f price quantity))

(println (basic-item-total 10 20))
;(println (with-line-item-conditions basic-item-total 21 1));wrong
;(def item-total (partial with-line-item-conditions basic-item-total))
;(println (item-total 20 3))

(defn total-cost
  ([item-cost number-of-items] (* item-cost number-of-items))
  ([item-cost] (total-cost item-cost 1)))

(println (total-cost 10 5))

(defn total-all-number [& number] (apply + number))
(println (total-all-number 2 4 5))

(defn count-down [n]
  (if-not (zero? n)
    (do
      (if (= 0 (rem n 100))
        (println "count-down:" n))
      (count-down (dec n)))))

(count-down 10000)                                          ;when params is 100000,system shall occur stackoverflow exception

(defn count-downr [n]
  (if-not (zero? n)
    (do
      (if (= 0 (rem n 100))
        (println "count-downr:" n))
      (recur (dec n)))))

(count-downr 100000)
