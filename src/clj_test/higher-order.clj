(defn twice [x]
  (println "original function")
  (* 2 x))

(defn call-twice [y] (twice y))

(defn with-log [function-to-call log-statement]
  (fn [& args]
    (println log-statement)
    (apply function-to-call args)))

(call-twice 10)

(let [aa (with-log twice "call the twice function")]
  (aa 20))
