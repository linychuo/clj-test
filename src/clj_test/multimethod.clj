(defn fee-amount [percentage user]
  (float (* 0.01 percentage (:salary user))))

(defn affiliate-fee-cond [user]
  (cond
    (= :google.com (:referrer user)) (fee-amount 0.01 user)
    (= :mint.com (:referrer user)) (fee-amount 0.03 user)
    :default (fee-amount 0.02 user)))

(def user {:referrer :google.com :salary 300})
(affiliate-fee-cond user)

(println "-------------------------------")
(defmulti affiliate-fee :referrer)
(defmethod affiliate-fee :mint.com [user]
  (fee-amount 0.03 user))
(defmethod affiliate-fee :google.com [user]
  (fee-amount 0.01 user))
(defmethod affiliate-fee :default [user]
  (fee-amount 0.02 user))
(def user1 {:referrer :mint.com :salary 100})
(def user2 {:referrer :google.com :salary 200})
(def user3 {:referrer :yahoo.com :salary 300})

;(affiliate-fee user1)
;(affiliate-fee user2)
;(affiliate-fee user3)
