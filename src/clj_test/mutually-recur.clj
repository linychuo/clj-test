(declare hat)

(defn cat [n]
  (if-not (zero? n)
    (do
      (if (= 0 (rem n 100))
        (println "cat:" n))
      (hat (dec n)))))

(defn hat [n]
  (if-not (zero? n)
    (do
      (if (= 0 (rem n 100))
        (println "hat:" n))
      (cat (dec n)))))

;(cat 100000)
;(hat 100000)

(declare hatt)

(defn catt [n]
  (if-not (zero? n)
    (do
      (if (= 0 (rem n 100))
        (println "catt:" n))
      #(hatt (dec n)))))

(defn hatt [n]
  (if-not (zero? n)
    (do
      (if (= 0 (rem n 100))
        (println "hatt:" n))
      #(catt (dec n)))))

(trampoline catt 100000)
