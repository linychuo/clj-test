(ns clj_test.ref-test)

(import 'java.util.Random)

(def N1 (ref 0))
(def N2 (ref 1))

(defn writer []
  (let [n (.nextInt (Random.))]
    (dosync (ref-set N1 n)
            (ref-set N2 (inc n)))
    (println "writer ...")
    (Thread/sleep 100)
    (recur)))

(defn reader []
  (if (not (= 1 (dosync (- @N2 @N1))))
    (println "______________________")                      ;never print
    (do
      (println "reader ...")
      (Thread/sleep 100)
      (recur))))

(defn start-all-threads []
  (let [t1 (Thread. writer)
        t2 (Thread. reader)]
    (.start t1)
    (.start t2)
    (.join t1)
    (.join t2)))

(start-all-threads)