(ns clj-test.server)

(import '(java.util StringTokenizer)
        '(java.net ServerSocket Socket SocketException)
        '(java.io PrintWriter InputStreamReader BufferedReader))

(def storage (atom {}))
(def exit? (atom false))
(def parsing-re #"([A-Z]+)\s*(\S+)\s*(\S*)")

(defn accept-connection [server-socket]
  (try (. server-socket accept)
       (catch SocketException e)))

(defmulti operate (fn [command & _]
                    (keyword command)))

(defmethod operate :GET [c key & _]
  (if (contains? @storage key)
    (str key " = " (@storage key))
    "No such key found"))

(defmethod operate :PUT [c key value & _]
  (do (swap! storage assoc key value)
      (str "Value of " key " added")))

(defmethod operate :DELETE [c key & _]
  (if (contains? @storage key)
    (do (swap! storage dissoc key)
        (str "Value of " key " deleted"))
    "No such key found"))

(defmethod operate :EXIT [& _]
  (reset! exit? true))

(defmethod operate :default [& _]
  "Wrong command")

(defn parse-and-operate [s]
  (->> s
       (re-find parsing-re)
       rest
       (apply operate)))


(defn start-server [port]
  (with-open [s-socket (new ServerSocket port)
              c-socket (accept-connection s-socket)
              in (new BufferedReader (new InputStreamReader (. c-socket getInputStream)))
              out (new PrintWriter (. c-socket getOutputStream) true)]
    (do (reset! exit? false)
        (while (not @exit?)
          (->> (. in readLine)
               parse-and-operate
               (. out println))))))
(start-server 1234)
