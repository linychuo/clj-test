(def ^:dynamic factor 10)

(defn multipy [x]
  (* x factor))
;(map multipy [1 2 3 4 5])
;result (10 20 30 40 50)

;(binding [factor 20]
;  (doall (map multipy [1 2 3 4 5]))))
;result (20 40 60 80 100)

(let [factor 20]
  (doall (map multipy [1 2 3 4 5])))
;result (10 20 30 40 50)

;Only the binding form can affect the dynamic scope of vars.